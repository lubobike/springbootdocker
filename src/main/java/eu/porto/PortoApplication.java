package eu.porto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class PortoApplication {

    @RequestMapping("/")
    public String home() {
        return "Hello Docker World, author is Lubo Sladok";
    }

    public static void main(String[] args) {
        SpringApplication.run(PortoApplication.class, args);
    }

}
